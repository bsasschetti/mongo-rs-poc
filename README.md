MongoDB ReplicaSet PoC w/Docker
===============================

Simple proof of concept for testing ReplicaSet behaviour with one PRIMARY and two SECONDARY instances, everything managed inside a 'Dockerized' architecture.